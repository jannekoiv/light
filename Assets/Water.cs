using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Unity.Mathematics;
using UnityEngine;

public class Water : MonoBehaviour
{
    void Start()
    {
        _mesh = GetComponent<MeshFilter>().mesh = new Mesh();
        _vertices = new Vector3[(_xSubdivs + 1) * (_zSubdivs + 1)];

        int[] triangles = new int[_xSubdivs * _zSubdivs * 6];
        for (int ti = 0, vi = 0, y = 0; y < _zSubdivs; y++, vi++)
        {
            for (int x = 0; x < _xSubdivs; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + _xSubdivs + 1;
                triangles[ti + 5] = vi + _xSubdivs + 2;
            }
        }

        _uv = new Vector2[_vertices.Length];
        for (int i = 0, z = 0; z <= _zSubdivs; z++)
        {
            for (int x = 0; x <= _xSubdivs; x++, i++)
            {
                _uv[i] = new Vector2(x / (float)_xSubdivs, z / (float)_zSubdivs * 1.0f);
            }
        }

        _mesh.vertices = _vertices;
        _mesh.triangles = triangles;
        _mesh.uv = _uv;
    }

    void FixedUpdate()
    {
        const int defaultSubdivs = 10;
        const float xScale = (float)defaultSubdivs / (float)_xSubdivs;
        const float zScale = (float)defaultSubdivs / (float)_zSubdivs;
        const float xDensity = 2.0f;
        const float zDensity = 2.0f;
        const float xSpeed = 2.0f;
        const float zSpeed = 1.0f;
        const float xAmplitude = 0.03f;
        const float zAmplitude = 0.03f;

        for (int iz = 0, i = 0; iz <= _zSubdivs; iz++)
        {
            for (int ix = 0; ix <= _xSubdivs; ix++, i++)
            {
                float x = (ix - _xSubdivs / 2.0f) * xScale;
                float z = (iz - _zSubdivs / 2.0f) * zScale;
                float xTime = Time.fixedTime * xSpeed;
                float zTime = Time.fixedTime * zSpeed;

                float y = (float)(Math.Cos(x * xDensity + xTime) * xAmplitude) +
                          (float)(Math.Sin(z * zDensity + zTime) * zAmplitude) +
                          (float)(Math.Cos(z * zDensity * 3.0f + zTime * 3.0f) * zAmplitude / 3.0f) +
                          (float)(Math.Sin(z * zDensity * 3.0f + zTime * 3.0f) * zAmplitude / 3.0f);

                // xTime += xSpeed;
                // zTime += zSpeed;
                _vertices[i] = new Vector3(x, y, z);
            }
        }
        _mesh.vertices = _vertices;
        _mesh.uv = _uv;
        _mesh.RecalculateNormals();
        _mesh.RecalculateBounds();
        transform.position = transform.position + new Vector3(0.0f, _risingSpeed, 0.0f);
    }

    private Vector3[] _vertices;
    private Vector2[] _uv;
    const int _xSubdivs = 100;
    const int _zSubdivs = 100;
    Mesh _mesh;
    [SerializeField]
    float _risingSpeed;
}
