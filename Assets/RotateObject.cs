using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    void Update() {
        if(Input.GetKey(KeyCode.A)) {
            angle.y -= 0.1f;
        }
        else if(Input.GetKey(KeyCode.D)) {
            angle.y += 0.1f;
        }
        if(Input.GetKey(KeyCode.W)) {
            angle.x -= 0.1f;
        }
        else if(Input.GetKey(KeyCode.S)) {
            angle.x += 0.1f;
        }

        Debug.Log(angle);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        angle.y += 1.0f;
        transform.rotation = Quaternion.Euler(angle);
    }

    Vector3 angle;
}
