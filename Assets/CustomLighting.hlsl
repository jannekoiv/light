#ifndef CUSTOM_LIGHTING_INCLUDED
#define CUSTOM_LIGHTING_INCLUDED

struct CustomLightingData {
    float3 normalWS;
    float3 viewDirectionWS;
    float3 albedo;
    float smoothness;
};

float smoothnessPower(float smoothness)
{
    return exp2(10 * smoothness + 1.0);
}

#ifndef SHADERGRAPH_PREVIEW
float3 CustomLightHandling(CustomLightingData d, Light light) 
{
    float3 radiance = light.color;
    float diffuse = saturate(dot(d.normalWS, light.direction));
    float specularDot = saturate(dot(d.normalWS, normalize(light.direction + d.viewDirectionWS)));
    float specular = pow(specularDot, smoothnessPower(d.smoothness)) * diffuse;
    float3 color = d.albedo * radiance * (diffuse + specular);
    return color;
}
#endif

float3 CalculateCustomLighting(CustomLightingData d)
{
#ifndef SHADERGRAPH_PREVIEW
    Light mainLight = GetMainLight();
    float3 color = 0;
    color += CustomLightHandling(d, mainLight);
    return color;
#else
    return d.albedo;
#endif
}


void CalculateCustomLighting_float(float3 Normal, float3 ViewDirection, float3 Albedo, float Smoothness, out float3 Color)
{
    CustomLightingData d;
    d.normalWS = Normal;
    d.viewDirectionWS = ViewDirection;
    d.albedo = Albedo;
    d.smoothness = Smoothness;
    Color = CalculateCustomLighting(d);
}

#endif